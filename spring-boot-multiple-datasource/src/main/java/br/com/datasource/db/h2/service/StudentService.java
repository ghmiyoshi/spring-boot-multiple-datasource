package br.com.datasource.db.h2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.datasource.db.h2.entity.Student;
import br.com.datasource.db.h2.repository.StudentRepository;

@Service
public class StudentService {

	@Autowired
	private StudentRepository repository;

	public List<Student> findAllStudents() {
		return this.repository.findAll();
	}

}
