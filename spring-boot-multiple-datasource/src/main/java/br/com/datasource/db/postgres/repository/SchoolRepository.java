package br.com.datasource.db.postgres.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.datasource.db.postgres.entity.School;

@Repository
public interface SchoolRepository extends JpaRepository<School, Long> {

}
