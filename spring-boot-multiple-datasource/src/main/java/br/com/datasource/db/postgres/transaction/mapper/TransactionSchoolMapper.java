package br.com.datasource.db.postgres.transaction.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import br.com.datasource.db.postgres.entity.School;

public class TransactionSchoolMapper implements RowMapper<School> {

	@Override
	public School mapRow(ResultSet resultSet, int rowNum) throws SQLException {
		return School.builder()
				.id(resultSet.getLong("id"))
				.name(resultSet.getString("name"))
				.address(resultSet.getString("address"))
				.build();
	}

}
