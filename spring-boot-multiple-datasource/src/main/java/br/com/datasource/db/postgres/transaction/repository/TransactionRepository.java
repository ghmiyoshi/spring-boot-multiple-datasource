package br.com.datasource.db.postgres.transaction.repository;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import br.com.datasource.db.postgres.entity.School;
import br.com.datasource.db.postgres.transaction.entity.Count;
import br.com.datasource.db.postgres.transaction.mapper.TransactionCountMapper;
import br.com.datasource.db.postgres.transaction.mapper.TransactionSchoolMapper;
import br.com.datasource.db.postgres.transaction.queries.TransactionQueries;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class TransactionRepository {

	@Autowired
	@Qualifier(value = "jdbcPostgres")
	private JdbcTemplate template;

	@Autowired
	private TransactionQueries query;

	public Count getCount() {
		log.info("Connecting in postgres {}", LocalDateTime.now());

		try {
			String sql = this.query.count();

			log.info("Executing query {}", sql);

			return template.queryForObject(sql, new TransactionCountMapper());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}

		return null;
	}

	public School getSchool(Integer id) {
		log.info("Connecting in postgres {}", LocalDateTime.now());

		try {
			NamedParameterJdbcTemplate namedParameter = new NamedParameterJdbcTemplate(template);
            MapSqlParameterSource param = new MapSqlParameterSource().addValue("id", id);
			
			String sql = this.query.findSchoolById();
			
			log.info("Executing query {}", sql);

			return namedParameter.queryForObject(sql, param, new TransactionSchoolMapper());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}

		return null;
	}

}
