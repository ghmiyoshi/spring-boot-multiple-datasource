package br.com.datasource.db.postgres.transaction.queries;

import org.springframework.stereotype.Service;

import lombok.Data;

@Data
@Service
public class TransactionQueries {

	public String count() {
		String sql = "SELECT COUNT(*) FROM school";

		return sql;
	}

	public String findSchoolById() {
		String sql = "SELECT * FROM school WHERE id = :id";

		return sql;
	}

}
