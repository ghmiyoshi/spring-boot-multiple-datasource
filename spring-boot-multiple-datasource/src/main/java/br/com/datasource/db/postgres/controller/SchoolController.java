package br.com.datasource.db.postgres.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.datasource.db.postgres.entity.School;
import br.com.datasource.db.postgres.transaction.entity.Count;
import br.com.datasource.db.postgres.transaction.repository.TransactionRepository;

@RestController
@RequestMapping("/school")
public class SchoolController {

	@Autowired
	private TransactionRepository transactionRepository;

	@GetMapping("/count")
	public ResponseEntity<Count> getCount() {
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(this.transactionRepository.getCount());
	}

	@GetMapping("/findId/{id}")
	public ResponseEntity<School> getSchoolEntity(@PathVariable("id") String id) {
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(this.transactionRepository.getSchool(Integer.parseInt(id)));
	}

}
