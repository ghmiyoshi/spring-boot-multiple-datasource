package br.com.datasource.db.h2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.datasource.db.h2.entity.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

}
