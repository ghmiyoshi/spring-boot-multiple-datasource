package br.com.datasource.db.postgres.transaction.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import br.com.datasource.db.postgres.transaction.entity.Count;

public class TransactionCountMapper implements RowMapper<Count> {

	@Override
	public Count mapRow(ResultSet resultSet, int rowNum) throws SQLException {
		return Count.builder()
				.count(resultSet.getInt("count"))
				.build();
	}

}
