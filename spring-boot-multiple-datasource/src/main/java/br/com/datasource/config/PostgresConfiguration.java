package br.com.datasource.config;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

import br.com.datasource.db.postgres.entity.School;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "br.com.datasource.db.postgres.repository", entityManagerFactoryRef = "postgresEntityManagerFactory", transactionManagerRef = "postgresTransactionManager")
public class PostgresConfiguration {

	@Bean
	@ConfigurationProperties(prefix = "postgres.datasource")
	public DataSourceProperties postgresDataSourceProperties() {
		return new DataSourceProperties();
	}

	
	@Bean("postgresDatasource")
	public DataSource postgresDataSource() {
		return postgresDataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
	}
	
	@Bean("jdbcPostgres")
	public JdbcTemplate jdbcTemplate(@Qualifier("postgresDatasource") DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}

	@Bean(name = "postgresEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean postgresEntityManagerFactory(EntityManagerFactoryBuilder builder, @Qualifier("postgresDatasource") DataSource dataSource) {
		// Properties example
		HashMap<String, Object> properties = new HashMap<String, Object>();
		// properties.put("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
        // properties.put("hibernate.connection.driver_class", "oracle.jdbc.OracleDriver");
		properties.put("hibernate.hbm2ddl.auto", "update");
		
		return builder.dataSource(dataSource).packages(School.class).properties(properties).build();
	}

	@Bean(name = "postgresTransactionManager")
	public PlatformTransactionManager postgresTransactionManager(
			@Qualifier("postgresEntityManagerFactory") LocalContainerEntityManagerFactoryBean entityManagerFactoryBean) {
		return new JpaTransactionManager(entityManagerFactoryBean.getObject());
	}
}
