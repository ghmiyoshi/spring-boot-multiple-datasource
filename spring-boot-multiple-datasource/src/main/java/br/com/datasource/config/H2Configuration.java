package br.com.datasource.config;

import java.util.HashMap;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

import br.com.datasource.db.h2.entity.Student;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
		basePackages = {"br.com.datasource.db.h2.repository", "br.com.datasource.db.h2.entity"}, 
		entityManagerFactoryRef = "h2EntityManager",
		transactionManagerRef = "h2TransactionManager"
		)
public class H2Configuration {

	// creates data-source properties bean with h2 database details

	@Bean
	@Primary
	@ConfigurationProperties(prefix = "h2.datasource")
	public DataSourceProperties h2DataSourceProperties() {
		return new DataSourceProperties();
	}

	// creates data-source bean

	@Bean(name = "h2DataSource")
	@Primary
	public DataSource h2DataSource() {
		return h2DataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
	}

	// creates entity manager with scanned entity classes of h2 database
	
	@Bean(name = "h2EntityManager")
	@Primary
	public LocalContainerEntityManagerFactoryBean h2EntityManager(EntityManagerFactoryBuilder builder, @Qualifier("h2DataSource") DataSource h2DataSource) {
		// Properties example
		HashMap<String, Object> properties = new HashMap<String, Object>();
		//properties.put("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
        //properties.put("hibernate.connection.driver_class", "oracle.jdbc.OracleDriver");
		properties.put("hibernate.hbm2ddl.auto", "update");
		
		return builder.dataSource(h2DataSource).packages(Student.class).properties(properties).build();
	}

	@Bean(name = "h2TransactionManager")
	@Primary
	public PlatformTransactionManager h2TransactionManager(@Qualifier("h2EntityManager") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}
}
